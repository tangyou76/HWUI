package com.hw.droid.hwcatalog;

import hwdroid.app.HWActivity;
import hwdroid.widget.ItemAdapter;
import hwdroid.widget.item.Item;
import hwdroid.widget.item.SeparatorItem;
import hwdroid.widget.item.TextItem;
import hwdroid.widget.item.WidgetText2Item;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;

public class WidgetItemActivity extends HWActivity {

	List<Item> items = new ArrayList<Item>();
	ItemAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setActivityContentView(R.layout.listview_layout);
		ListView listview = (ListView) this.findViewById(R.id.listview);

		for (int i = 0; i < 100; i++) {
			if (i == 5 || i == 10 || i == 15 || i == 20 || i == 25) {
				items.add(new SeparatorItem("drawable item"));
				continue;
			}

			LinearLayout rightLayout = new LinearLayout(this);
			LinearLayout leftLayout = new LinearLayout(this);
			rightLayout.setOrientation(LinearLayout.VERTICAL);
			leftLayout.setOrientation(LinearLayout.VERTICAL);
			rightLayout.removeAllViews();

			Switch ss1 = new Switch(this);
			ss1.setTag(i);
			ss1.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
					int tag = (Integer) arg0.getTag();

					TextItem item = (TextItem) items.get(tag);
					if (arg1)
						item.mSubText = "open..........";
					else
						item.mSubText = "close";

					mAdapter.notifyDataSetChanged();

				}
			});

			rightLayout.addView(ss1);
			leftLayout.removeAllViews();

			Switch ss = new Switch(this);
			ss.setTag(i);
			ss.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
					int tag = (Integer) arg0.getTag();

					TextItem item = (TextItem) items.get(tag);
					if (arg1)
						item.mText = "open..........";
					else
						item.mText = "close";

					mAdapter.notifyDataSetChanged();

				}
			});
			leftLayout.addView(ss);

			items.add(new WidgetText2Item("close", "close", leftLayout,
					rightLayout, R.id.action_bar_refresh));
		}

		mAdapter = new ItemAdapter(this, items);
		listview.setAdapter(mAdapter);
	}
}

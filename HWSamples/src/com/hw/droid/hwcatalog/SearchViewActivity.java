package com.hw.droid.hwcatalog;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.ListView;
import android.widget.SearchView.OnCloseListener;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Toast;

import hwdroid.app.HWActivity;
import hwdroid.widget.ItemAdapter;
import hwdroid.widget.ActionBar.ActionBarView;
import hwdroid.widget.ActionBar.ActionBarView.ActionBarSearchViewListener;
import hwdroid.widget.ActionBar.ActionBarView.OnBackKeyItemClick;
import hwdroid.widget.item.Item;
import hwdroid.widget.item.TextItem;

import java.util.ArrayList;
import java.util.List;

public class SearchViewActivity extends HWActivity {
    private ActionBarView mActionBarView;
    
    private Toast mToast;
    
    ListView mListView;
    List<Item> mItems;
    ItemAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContentView(R.layout.activity_searchview);
        mActionBarView = (ActionBarView) findViewById(R.id.actionbarview);
        mActionBarView.showBackKey(true, new OnBackKeyItemClick() {
            @Override
            public void onBackKeyItemClick() {
                SearchViewActivity.this.finish();
            }
        });
        
        mActionBarView.setTitle("SearchView Activity");
        
        mToast = new Toast(this);
        mListView = (ListView)findViewById(R.id.list); 
        mItems = new ArrayList<Item>(); 
        mAdapter = new ItemAdapter(this, mItems);
        mListView.setAdapter(mAdapter);
        
        for(int i = 0; i < 30; i++) {
            mAdapter.add(new TextItem("search item."));
        } 
            
        //show search view
        mActionBarView.enableSearchView(true);
        mActionBarView.setQuery("set query");
        mActionBarView.setQueryHint("set query hint");
        mActionBarView.setOnQueryTextListener(new OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                showToast("onQueryTextSubmit() has been called");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.i(SearchViewActivity.class.getName(), "onQueryTextChange() : " + newText);
                return false;
            }
        });

        mActionBarView.setOnCloseListener(new OnCloseListener() {

            @Override
            public boolean onClose() {
                showToast("onClose() has been called");
                return false;
            }
        });

        mActionBarView.setSearchViewListener(new ActionBarSearchViewListener(){

            @Override
            public void startOutAnimation(int time) {
                showToast("startOutAnimation");
            }

            @Override
            public void startInAnimation(int time) {
                showToast("startInAnimation");
            }

            @Override
            public void doTextChanged(CharSequence s) {
                
                mAdapter.clear();
                
                if(s != null && s.length() > 0) {
                    
                    for(int i = 0; i < 30; i++) {
                        mAdapter.add(new TextItem(s.toString()));
                    }
                   
                } else {
                    for(int i = 0; i < 30; i++) {
                        mAdapter.add(new TextItem("search item."));
                    } 
                }
                
                mAdapter.notifyDataSetChanged();
                
            }});
    }
    
    private void showToast(CharSequence text) {
        TextView t = new TextView(this);
        t.setBackgroundColor(Color.RED);
        t.setText(text);
        t.setTextColor(Color.BLACK);
        mToast.setView(t);   
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.TOP, 0, 0);
        mToast.show();
    }
}

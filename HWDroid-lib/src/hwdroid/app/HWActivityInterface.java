
package hwdroid.app;

import android.view.View;
import android.widget.FrameLayout;

import hwdroid.widget.ActionBar.ActionBarView.OnLeftWidgetItemClick;
import hwdroid.widget.ActionBar.ActionBarView.OnLeftWidgetItemClick2;
import hwdroid.widget.ActionBar.ActionBarView.OnOptionMenuClick;
import hwdroid.widget.ActionBar.ActionBarView.OnRightWidgetItemClick;
import hwdroid.widget.ActionBar.ActionBarView.OnRightWidgetItemClick2;
import hwdroid.widget.ActionBar.ActionBarView.OnTitle2ItemClick;

public interface HWActivityInterface {

    static final String HW_ACTION_BAR_TITLE       = "HWActivityInterface.HW_ACTION_BAR_TITLE";

    FrameLayout getContentView();
    int createLayout();
    void onPreContentChanged();
    void onPostContentChanged();
    void setOptionTitle2(CharSequence title);
    void setOptionItems(CharSequence[] optionItem, OnOptionMenuClick click);
    void setTitle2(CharSequence title);
    void setTitle2(CharSequence title, OnTitle2ItemClick click);
    void setSubTitle2(CharSequence title);
    void showBackKey(boolean show);
    void onBackKey();
    void setLeftWidgetView(View v);
    void setRightWidgetView(View v);
    void setRightWidgetView(View v, boolean clickable);
    void removeLeftWidgetView();
    void removeRightWidgetView();
    void setLeftWidgetClickListener(OnLeftWidgetItemClick click);
    void setRightWidgetClickListener(OnRightWidgetItemClick click);
    void setLeftWidgetClickListener2(OnLeftWidgetItemClick2 click);
    void setRightWidgetClickListener2(OnRightWidgetItemClick2 click);
    void setLeftWidgetItemEnabled(boolean enabled);
    void setRightWidgetItemEnabled(boolean enabled);
    boolean isLeftWidgetItemEnabled();
    boolean isRightWidgetItemEnabled();
}
